/**
 * Chat conversation for CSC 580 chat program. Objects in this class represent
 * active chat conversations, and interact with the chat hub for communication
 * and a ChatView object to initiate/report chat actions. Note that this class
 * must be initialized with the setChatViewController static method before any
 * Conversation objects can be created.
 * 
 * @version 0.1
 * @author srtate
 */
public class Conversation {
    public final static int STATUS_NOTCONN = 0;
    public final static int STATUS_CONN = 1;

    // One chat view controller for all conversations
    private static ChatViewController cvc;
    
    protected int id;
    protected int status;
    protected final String otherID;
    protected final HubSession hubConn;
    protected final ChatView view;

    /**
     * Initialize Conversation class with an object that allows for views to
     * be created appropriate to the application.
     * @param cvc_in
     */
    public static void setChatViewController(ChatViewController cvc_in) {
        cvc = cvc_in;
    }
    
    /**
     * Create a new conversation for an already-established conversation (one
     * that already has a session id from the chat hub).
     * @param hub the logged-in chat hub session
     * @param sessionID the already-established session id
     * @param connTo the username on the other end of the connection
     */
    public Conversation(HubSession hub, int sessionID, String connTo) {
        status = STATUS_CONN;
        id = sessionID;
        otherID = connTo;
        hubConn = hub;
        view = cvc.newView(this);
    }

    /**
     * Create a new conversation object for a conversation that hasn't been
     * connected yet. This is created when the user makes a request for a new
     * chat conversation, but when the connection hasn't been acknowledged (and
     * a session id assigned) by the chat hub. Before this can be used, the
     * connection must be completed, and establish() must be called.
     * @param hub the logged-in chat hub session
     * @param sessionID the already-established session id
     * @param connTo the username on the other end of the connection
     */
    public Conversation(HubSession hub, String connTo) {
        status = STATUS_NOTCONN;
        hubConn = hub;
        otherID = connTo;
        view = cvc.newView(this);
        hub.connectRequest(this);
    }

    /**
     * Establish a session. This is called when a connection request is
     * completed and acknowledged by the chat hub.
     * @param id the session ID for this conversation
     */
    public void establish(int id) {
        this.id = id;
        this.status = STATUS_CONN;
        view.setConnStatus(true);
        view.addInfoMessage("*** CONNECTED to " + otherID);
    }

    /**
     * Call this when a connection attempt has been made, creating the
     * Conversation object, but the connection does not go through.
     */
    public void failed() {
        view.addInfoMessage("*** CONNECT ATTEMPT FAILED");
        view.disconnectConvo();
    }

    /**
     * Drop an active connection. Conversation object can't be use do send
     * or receive messages after this, unless a new conversation is established
     * through a call to establish().
     */
    public void drop() {
        if (STATUS_CONN == status) {
            hubConn.dropConvo(id);
            this.id = -1;
            this.status = STATUS_NOTCONN;
            view.setConnStatus(false);
            view.addInfoMessage("*** DISCONNECTED");
        }
    }

    /**
     * Get the identity of the other side of the chat conversation.
     * @return the other user name
     */
    public String getOtherID() {
        return otherID;
    }

    /**
     * Process a message that has been received as part of a conversation.
     * Basically just sends the message on to the view so it can be displayed
     * or processed.
     * 
     * TODO: Make this into a secure message receiver, which will have to
     * decrypt the message before sending the recovered plaintext to the view.
     * 
     * @param message the message that was received
     */
    public void received(String message) {
        view.addReceivedMessage(this, message);
    }

    /**
     * Send a message on an established conversation. Tells the hub to actually
     * transmit the message.
     * 
     * TODO: Make this into a secure message sender, which encrypts the plaintext
     * message before sending it to the hub.
     * 
     * @param message the message to send
     */
    public void sendMessage(String message) {
        if (STATUS_CONN == status) {
            hubConn.sendMessage(id, message);
        }
    }
}
