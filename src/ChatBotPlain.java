import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A simple chatbot "view". This chatbot simple repeats any message you send it,
 * prefixed with "Got" (and putting the message in quotes). Really just useful
 * for testing connetivity and seeing what messages you are actually sending.
 * 
 * @version 0.1
 * @author srt
 */
public class ChatBotPlain implements ChatViewController {
    class ChatBotLogic implements ChatView {
        @Override
        public void setConnStatus(boolean connected) {
        }

        @Override
        public void addInfoMessage(String info) {
        }

        @Override
        public void addReceivedMessage(Conversation convo, String msg) {
            convo.sendMessage("Got \""+msg+"\"");
        }

        @Override
        public void disconnectConvo() {
        }
        
    }

    @Override
    public ChatView newView(Conversation convo) {
        return new ChatBotLogic();
    }
    
    public static void main(String argv[]) {
        Conversation.setChatViewController(new ChatBotPlain());
        HubSession hub = new HubSession(new LoginCredentials("chatbot", "password"));
        try {
            Thread.sleep(2000000000);  // TODO: This isn't the right way todo this...
        } catch (InterruptedException ex) {
            Logger.getLogger(ChatBotPlain.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    
}
