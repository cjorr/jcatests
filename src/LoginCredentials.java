/**
 * Login credentials manages information necessary to log in to the chat hub.
 * With version 0.1 it is just a username and a password, but this will be
 * updated for more advanced authentication methods later.
 * 
 * @version 0.1
 * @author srt
 */
public class LoginCredentials {
    private String userID;
    private String password;
    
    public LoginCredentials(String userID, String password) {
        this.userID = userID;
        this.password = password;
    }
    
    public String getUserID() {
        return userID;
    }
    
    public String answerChallenge(String challenge) {
        return password;
    }   
}
