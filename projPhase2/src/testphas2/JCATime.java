package testphas2;

import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Random;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

/**
 *
 * @author CyberJail
 */



public class JCATime {
    
    final static int byteSize=100000;
    
    
    public static void main(String[] args) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, Exception {
        
        final String AES="AES";
        final String DES="DES";
        final int aesKey=16;
        final int desKey=8;
        
        
        byte[] input=new byte[byteSize];
        byte[] strToEncrypt;
        String str="Actually Gollum lived on a slimy island of rock in the middle of thelake. He was watching Bilbo now from the distance with his pale eyes liketelescopes. Bilbo could not see him, but he was wondering a lot aboutBilbo, for he could see that he was no goblin at all.\n" +
"\n" +
"Gollum got into his boat and shot off from the island, while Bilbo was sitting on the brink altogether flummoxed and at the end of his way and his wits. Suddenly up came Gollum and whispered and hissed:\n" +
"\n" +
"\"Bless us and splash us, my precioussss! I guess it's a choice feast; atleast a tasty morsel it'd make us, gollum!\" And when he said gollum he madea horrible swallowing noise in his throat. That is how he got his name,though he always called himself 'my precious.'\n" +
"\n" +
"The hobbit jumped nearly out of his skin when the hiss came in his ears,and he suddenly saw the pale eyes sticking out at him.\n" +
"\n" +
"\"Who are you?\" he said, thrusting his dagger in front of him.\n" +
"\n" +
"\"What iss he, my preciouss?\" whispered Gollum (who always spoke tohimself through never having anyone else to speak to). This is what he hadcome to find out, for he was not really very hungry at the moment, onlycurious; otherwise he would have grabbed first and whispered afterwards.\n" +
"\n" +
"\"I am Mr. Bilbo Baggins. I have lost the dwarves and I have lost thewizard, and I don't know where I am; and I don't want to know, if only Ican get away.\"\n" +
"\n" +
"\"What's he got in his handses?\" said Gollum, looking at the sword, whichhe did not quite like.\n" +
"\n" +
"\"A sword, a blade which came out of Gondolin!\"\n" +
"\n" +
"\"Sssss,\" said Gollum, and became quite polite. \"Praps ye sits here andchats with it a bitsy, my preciousss. It like riddles, praps it does, doesit?\" He was anxious to appear friendly, at any rate for the moment, anduntil he found out more about the sword and the hobbit, whether he wasquite alone really, whether he was good to eat, and whether Gollum wasreally hungry. Riddles were all he could think of. Asking them, andsometimes guessing them, had been the only game he had ever played withother funny creatures sitting in their holes in the long, long ago, beforethe goblins came and he was cut off with his friends far into the mountains he lost all his friends and was driven away, alone, and crept down, down,into the dark under the mountains.";



        input=str.getBytes(StandardCharsets.UTF_8);
        
        //Block tests encryption throughput for AES
        long startTime = System.nanoTime();
        encryptString(input,AES,aesKey);
        long endTime = System.nanoTime();
        long duration = (endTime - startTime)/1000000;
        System.out.println("AES encryption finished in: "+duration+" ms");
        //Block tests encryption throughput for DES
        
        long startTime2 = System.nanoTime();
        encryptString(input,DES,desKey);
        long endTime2 = System.nanoTime();
        long duration2 = (endTime2 - startTime2)/1000000;
        System.out.println("DES encryption finished in: "+duration2+" ms");
        
        //Block tests key testing rate for AES for each block of text
        String[] arr=str.split(" ");
        System.out.println(arr.length);
        long startTime3 = System.nanoTime();
        keyTest(arr,AES,aesKey);
        long endTime3 = System.nanoTime();
        long duration3 = (endTime3 - startTime3)/1000000;
        System.out.println("AES key testing finished in: "+duration3+" ms");
        //Block tests key testing rate for DES for each block of text
        long startTime4 = System.nanoTime();
        keyTest(arr,DES,desKey);
        long endTime4 = System.nanoTime();
        long duration4 = (endTime4 - startTime4)/1000000;
        System.out.println("DES key testing finished in: "+duration4+" ms");
        
        
        
        
    }
    
    
    public static void encryptString(byte[] strToEncrypt,String cryptoType,int keySize) throws Exception{
        byte[] keyBytes=new byte[keySize];
        
        SecretKeySpec key= new SecretKeySpec(keyBytes,cryptoType);
        Cipher cipher= Cipher.getInstance(cryptoType);
        
        cipher.init(Cipher.ENCRYPT_MODE, key);
        for(int i=0;i<300000;i++){
            cipher.doFinal(strToEncrypt);
        }
         
}
    
    public static void keyTest(String[] arr, String cryptoType,int keySize) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException{
        
        byte[] keyBytes=new byte[keySize];
        final String alphabet = "01234";
        final int N = alphabet.length();

        
        Cipher cipher= Cipher.getInstance(cryptoType);
        
        for(int i=0;i<30000;i++){
            
        byte[] input=new byte[byteSize];
        for (String arr1 : arr) {
            
            String key="";
            Random r = new Random();
            for (int x = 0; x < keySize; x++) {
                key+=alphabet.charAt(r.nextInt(N));
            }
            keyBytes=key.getBytes(StandardCharsets.UTF_8);
            SecretKeySpec secretKey= new SecretKeySpec(keyBytes,cryptoType);
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            input = arr1.getBytes(StandardCharsets.UTF_8);
            cipher.doFinal(input);
        }
        }

    }
    

}

